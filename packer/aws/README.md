# packer/aws

# gitlab-runner

```bash
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "${TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --docker-privileged \
  --description "${ENV}-gitlab-runner" \
  --tag-list "${ENV},all" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"

```