locals {
  cluster_name = ""
}

module "vpc-e2fyi" {
  source = "git::https://gitlab.com/e2fyi/terraform-aws-eks.git//modules/vpc"

  vpc_name     = "e2fyi"
  cluster_name = local.cluster_name
  cidr_block   = "10.0.0.0/16"
  subnets_az_to_cidr = {
    ap-southeast-1a = "10.0.250.0/24"
    ap-southeast-1b = "10.0.251.0/24"
    ap-southeast-1c = "10.0.252.0/24"
  }
  subnets_az_with_nat = {
    ap-southeast-1a = false
    ap-southeast-1b = false
    ap-southeast-1c = false
  }
  enable_dns_hostnames = true        # required to resolve EKS endpoint
  cidr_block_for_ssh   = "0.0.0.0/0" # any IP can ssh into the public subnet

  tags = local.tags
}
